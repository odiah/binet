package com.sysomos.word2vec.words.vectorizer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.util.FeatureUtil;

import com.sysomos.text.documentiterator.DocumentIterator;
import com.sysomos.text.math.utils.MathUtils;
import com.sysomos.text.sentenceiterator.SentenceIterator;
import com.sysomos.text.tokenizer.Tokenizer;
import com.sysomos.text.tokenizer.factory.TokenizerFactory;
import com.sysomos.word2vec.text.utils.InvertedIndex;
import com.sysomos.word2vec.wordstore.VocabCache;

public class TfVectorizer extends BaseTextVectorizer implements Serializable {

	private static final long serialVersionUID = -2576064654396910324L;

	public TfVectorizer() {
	}

	protected TfVectorizer(VocabCache cache, TokenizerFactory tokenizerFactory,
			List<String> stopWords, int minWordFrequency,
			DocumentIterator docIter, SentenceIterator sentenceIterator,
			List<String> labels, InvertedIndex index, int batchSize,
			double sample, boolean stem, boolean cleanup) {
		super(cache, tokenizerFactory, stopWords, minWordFrequency, docIter,
				sentenceIterator, labels, index, batchSize, sample, stem,
				cleanup);
	}

	private double tfForWord(String word) {
		return MathUtils.tf(cache.wordFrequency(word));
	}

	@Override
	public DataSet vectorize(InputStream is, String label) {
		return new DataSet(tfForInput(is), FeatureUtil
				.toOutcomeVector(labels.indexOf(label), labels.size()));
	}

	@Override
	public DataSet vectorize(String text, String label) {
		INDArray tfidf = tfForInput(text);
		INDArray label2 = FeatureUtil.toOutcomeVector(labels.indexOf(label),
				labels.size());
		return new DataSet(tfidf, label2);
	}

	@Override
	public DataSet vectorize(File input, String label) {
		try {
			return vectorize(FileUtils.readFileToString(input), label);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public INDArray transform(String text) {
		return tfForInput(text);
	}

	@Override
	public DataSet vectorize() {
		return null;
	}

	private INDArray tfForInput(InputStream is) {
		try {
			String text = new String(IOUtils.toByteArray(is));
			return tfForInput(text);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private INDArray tfForInput(String text) {
		INDArray ret = Nd4j.create(1, cache.numWords());
		Tokenizer tokenizer = tokenizerFactory.create(text);
		List<String> tokens = tokenizer.getTokens();

		for (int i = 0; i < tokens.size(); i++) {
			int idx = cache.indexOf(tokens.get(i));
			if (idx >= 0)
				ret.putScalar(idx, tfForWord(tokens.get(i)));
		}
		return ret;
	}

	public static class Builder
			extends com.sysomos.word2vec.words.vectorizer.Builder {
		public TextVectorizer build() {

			return new TfVectorizer(cache, tokenizerFactory, stopWords,
					minWordFrequency, docIter, sentenceIterator, labels, index,
					batchSize, sample, stem, cleanup);
		}
	}
}
