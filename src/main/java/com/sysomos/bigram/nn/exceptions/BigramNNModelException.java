package com.sysomos.bigram.nn.exceptions;

public class BigramNNModelException extends Exception {

	private static final long serialVersionUID = -8506014166244143605L;

	public BigramNNModelException(String message) {
		super(message);
	}

	public BigramNNModelException(Throwable cause) {
		super(cause);
	}

}
