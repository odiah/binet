package com.sysomos.bigram.nn;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sysomos.text.sentenceiterator.FileSentenceIterator;
import com.sysomos.text.tokenizer.factory.DefaultTokenizerFactory;
import com.sysomos.word2vec.Word2Vec;
import com.sysomos.word2vec.loader.WordVectorSerializer;
import com.sysomos.word2vec.word.vectors.InMemoryLookupTable;
import com.sysomos.word2vec.word.vectors.WordVectors;

public class Word2VectTest {

	private static final String dataset = "src/main/resources/dataset";
	private Word2Vec word2Vec;
	private static final Logger LOG = LoggerFactory
			.getLogger(Word2VectTest.class);
	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {
		word2Vec = new Word2Vec.Builder().useAdaGrad(false).learningRate(0.025)
				.minLearningRate(1e-2).negativeSample(0)
				.iterate(new FileSentenceIterator(new File(dataset)))
				.tokenizerFactory(new DefaultTokenizerFactory())
				.minWordFrequency(10).saveVocab(true).build();
	}

	@Test
	public void evalTest() {
		try {
			word2Vec.fit();
			System.out.println(word2Vec.vocab().toString());
			InMemoryLookupTable table = (InMemoryLookupTable) word2Vec
					.lookupTable();
			table.getSyn0().diviRowVector(table.getSyn0().norm2(0));

			LOG.info("Evaluate model....");
			double sim = word2Vec.similarity("people", "money");
			LOG.info("Similarity between people and money: " + sim);
			Collection<String> similar = word2Vec.wordsNearest("day", 20);
			LOG.info("Similar words to 'day' : " + similar);
			LOG.info("Save vectors....");
			WordVectorSerializer.writeWordVectors(word2Vec, "words.txt");
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	@Test
	public void loadTest() {
		try {
			System.out.println("Loading ...");
			WordVectors wordVectors = WordVectorSerializer
					.loadTxtVectors(new File("words.txt"));
			System.out.println(wordVectors.vocab().toString());
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	@After
	public void tearDown() {
		word2Vec = null;
	}
}
